// Copyright 2019 Anders Lingfors
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this softwareand associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and /or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions :
//
// The above copyright noticeand this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BETTERGETTER_INCLUDE_BETTERGETTER_DECORATE_H
#define BETTERGETTER_INCLUDE_BETTERGETTER_DECORATE_H

#include <utility>

namespace BetterGetter
{

template <typename SourceType, typename DestinationType>
struct Decorate;

template <typename SourceType, typename DestinationType>
struct Decorate<const SourceType&, DestinationType>
{
    using type = const DestinationType&;
};

template <typename SourceType, typename DestinationType>
struct Decorate<SourceType&, DestinationType>
{
    using type = DestinationType&;
};

template <typename SourceType, typename DestinationType>
struct Decorate<const SourceType, DestinationType>
{
    using type = const DestinationType&&;
};

template <typename SourceType, typename DestinationType>
struct Decorate
{
    using type = DestinationType&&;
};

template <typename ReturnType, typename T, typename ReturnValue>
auto decorated(T&& t, ReturnValue&& returnValue) ->
    typename Decorate<T, ReturnType>::type
{
    return std::forward<ReturnValue>(returnValue);
}

} // namespace BetterGetter

#endif // BETTERGETTER_INCLUDE_BETTERGETTER_DECORATE_H
