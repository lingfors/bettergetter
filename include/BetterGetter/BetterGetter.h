// Copyright 2019 Anders Lingfors
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this softwareand associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and /or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions :
//
// The above copyright noticeand this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BETTERGETTER_INCLUDE_BETTERGETTER_BETTERGETTER_H
#define BETTERGETTER_INCLUDE_BETTERGETTER_BETTERGETTER_H

#include "Decorate.h"

#include <type_traits>
#include <utility>

#ifndef GETTER_PREFIX
#define GETTER_PREFIX m
#endif // GETTER_PREFIX

#ifndef GETTER_SUFFIX
#define GETTER_SUFFIX
#endif // GETTER_SUFFIX

#define GETTER_SIGNATURE(name) get##name##()

#define GETTER_COPY_SIGNATURE(name) get##name##Copy()

#define MEMBER_NAME_PASTER(prefix, name, suffix) prefix##name##suffix

#define MEMBER_NAME_EVALUATOR(prefix, name, suffix)                            \
    MEMBER_NAME_PASTER(prefix, name, suffix)

#define MEMBER_NAME(name)                                                      \
    MEMBER_NAME_EVALUATOR(GETTER_PREFIX, name, GETTER_SUFFIX)

#define MEMBER_TYPE(name) decltype(MEMBER_NAME(name))

#define LVALUE_GETTER_BY_REF(member)                                           \
    decltype(auto) GETTER_SIGNATURE(member)&                                   \
    {                                                                          \
        return BetterGetter::decorated<MEMBER_TYPE(member)>(                   \
            *this, MEMBER_NAME(member));                                       \
    }

#define LVALUE_GETTER_DELETED(member) void GETTER_SIGNATURE(member)& = delete;

#define LVALUE_GETTER_BY_VALUE(member)                                         \
    auto GETTER_SIGNATURE(member)&                                             \
    {                                                                          \
        return BetterGetter::decorated<MEMBER_TYPE(member)>(                   \
            *this, MEMBER_NAME(member));                                       \
    }

#define LVALUE_GETTER_BY_VALUE_EXPLICIT_COPY(member)                           \
    LVALUE_GETTER_DELETED(member)                                              \
    auto GETTER_COPY_SIGNATURE(member) const&                                  \
    {                                                                          \
        return BetterGetter::decorated<MEMBER_TYPE(member)>(                   \
            *this, MEMBER_NAME(member));                                       \
    }

#define CONST_LVALUE_GETTER_BY_REF(member)                                     \
    decltype(auto) GETTER_SIGNATURE(member) const&                             \
    {                                                                          \
        return BetterGetter::decorated<MEMBER_TYPE(member)>(                   \
            *this, MEMBER_NAME(member));                                       \
    }

#define RVALUE_GETTER_BY_REF(member)                                           \
    decltype(auto) GETTER_SIGNATURE(member)&&                                  \
    {                                                                          \
        return BetterGetter::decorated<MEMBER_TYPE(member)>(                   \
            std::move(*this), std::move(MEMBER_NAME(member)));                 \
    }

#define RVALUE_GETTER_DELETED(member) void GETTER_SIGNATURE(member)&& = delete;

#define RVALUE_GETTER_BY_VALUE(member)                                         \
    auto GETTER_SIGNATURE(member)&&                                            \
    {                                                                          \
        return BetterGetter::decorated<MEMBER_TYPE(member)>(                   \
            std::move(*this), std::move(MEMBER_NAME(member)));                 \
    }

#define RVALUE_GETTER_BY_VALUE_EXPLICIT_COPY(member)                           \
    RVALUE_GETTER_DELETED(member)                                              \
    auto GETTER_COPY_SIGNATURE(member)&&                                       \
    {                                                                          \
        return BetterGetter::decorated<MEMBER_TYPE(member)>(                   \
            std::move(*this), std::move(MEMBER_NAME(member)));                 \
    }

#define CONST_RVALUE_GETTER_BY_REF(member)                                     \
    decltype(auto) GETTER_SIGNATURE(member) const&&                            \
    {                                                                          \
        return BetterGetter::decorated<decltype(MEMBER_NAME(member))>(         \
            std::move(*this), std::move(MEMBER_NAME(member)));                 \
    }

#define CONST_RVALUE_GETTER_BY_VALUE(member)                                   \
    auto GETTER_SIGNATURE(member) const&&                                      \
    {                                                                          \
        return BetterGetter::decorated<decltype(MEMBER_NAME(member))>(         \
            std::move(*this), std::move(MEMBER_NAME(member)));                 \
    }

#define CONST_RVALUE_GETTER_DELETED(member)                                    \
    void GETTER_SIGNATURE(member) const&& = delete;

#define CONST_RVALUE_GETTER_BY_VALUE_EXPLICIT_COPY(member)                     \
    CONST_RVALUE_GETTER_DELETED(member)                                        \
    auto GETTER_COPY_SIGNATURE(member) const&&                                 \
    {                                                                          \
        return BetterGetter::decorated<decltype(MEMBER_NAME(member))>(         \
            std::move(*this), std::move(MEMBER_NAME(member)));                 \
    }

#define GETTER_BY_REF(member)                                                  \
    LVALUE_GETTER_BY_REF(member)                                               \
    CONST_LVALUE_GETTER_BY_REF(member)                                         \
    RVALUE_GETTER_BY_REF(member)                                               \
    CONST_RVALUE_GETTER_BY_REF(member)

#define GETTER_BY_VALUE(member)                                                \
    LVALUE_GETTER_BY_VALUE(member)                                             \
    CONST_LVALUE_GETTER_BY_REF(member)                                         \
    RVALUE_GETTER_BY_VALUE(member)                                             \
    CONST_RVALUE_GETTER_BY_VALUE(member)

#define GETTER_BY_VALUE_EXPLICIT_COPY(member)                                  \
    LVALUE_GETTER_BY_VALUE_EXPLICIT_COPY(member)                               \
    CONST_LVALUE_GETTER_BY_REF(member)                                         \
    RVALUE_GETTER_BY_VALUE_EXPLICIT_COPY(member)                               \
    CONST_RVALUE_GETTER_BY_VALUE_EXPLICIT_COPY(member)

#define GETTER_BY_VALUE_NO_COPY(member)                                        \
    LVALUE_GETTER_DELETED(member)                                              \
    CONST_LVALUE_GETTER_BY_REF(member)                                         \
    RVALUE_GETTER_DELETED(member)                                              \
    CONST_RVALUE_GETTER_DELETED(member)

#endif // BETTERGETTER_INCLUDE_BETTERGETTER_BETTERGETTER_H
