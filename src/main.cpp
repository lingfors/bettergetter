// Copyright 2019 Anders Lingfors
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this softwareand associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and /or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions :
//
// The above copyright noticeand this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "BetterGetter/AsConst.h"
#include "BetterGetter/BetterGetter.h"

#include <iostream>

class Bar
{
public:
    Bar() noexcept
    {
    }

    Bar(const Bar&)
    {
        std::cout << "Copy" << std::endl;
    }

    Bar(Bar&&) noexcept
    {
        std::cout << "Move" << std::endl;
    }

    Bar& operator=(Bar other) noexcept
    {
        return *this;
    }
};

class Foo
{
public:
    GETTER_BY_VALUE(Bar);

private:
    Bar mBar;
};

int main(void)
{
    Foo foo1;
    const auto& i = foo1.getBar();

    const Foo foo2;
    auto& j = foo2.getBar();

    Foo foo3;
    const auto& k = std::move(foo3).getBar();

    const Foo foo4;
    const auto& m = std::move(foo4).getBar();

    return 0;
}
