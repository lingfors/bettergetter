// Copyright 2019 Anders Lingfors
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this softwareand associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and /or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions :
//
// The above copyright noticeand this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "BetterGetter/BetterGetter.h"

#include "BetterGetter/AsConst.h"

#include "gtest/gtest.h"

class Inner
{
public:
    GETTER_BY_REF(Int);

    void incInt()
    {
        ++mInt;
    }

private:
    int mInt = 0;
};

class GetterByRef : public testing::Test
{
public:
    Inner inner;
};

TEST_F(GetterByRef, ConstLvalue)
{
    AS_CONST(inner)
    {
        static_assert(std::is_same_v<const int&, decltype(inner.getInt())>);
    }
}

TEST_F(GetterByRef, NonConstLvalue)
{
    static_assert(std::is_same_v<int&, decltype(inner.getInt())>);

    const int& i = inner.getInt();
    const int j = i;
    ASSERT_EQ(i, j);
    inner.incInt();
    ASSERT_NE(i, j);
}

TEST_F(GetterByRef, ConstRvalue)
{
    AS_CONST(inner)
    {
        static_assert(
            std::is_same_v<const int&&, decltype(std::move(inner).getInt())>);
    }
}

TEST_F(GetterByRef, NonConstRvalue)
{
    static_assert(std::is_same_v<int&&, decltype(std::move(inner).getInt())>);

    const int& i = std::move(inner).getInt();
    const int j = i;
    ASSERT_EQ(i, j);
    inner.incInt();
    ASSERT_NE(i, j);
}
